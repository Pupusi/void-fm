# [Void-FM](http://void-fm.ga)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/cd0613ee12b141838727f1ea26fa4eed)](https://www.codacy.com/app/void-hikari/void-fm?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=void-hikari/void-fm&amp;utm_campaign=Badge_Grade)

虚空电台~ 来自二次元的旋律~


# 安装

- 创建数据库，导入 fm.sql

- 配置数据库配置文件 application/config/database.php

- 更改 application/views/Fm_view.php 里的标题 `<title>Void-FM</title>`


# 使用

**没有后台，直接操作数据库吧，用 phpMyAdmin 或 Adminer，向表 `void_fm` 添加数据**

`id` 是序号，从1开始。  例： `1`

`name` 是音乐名称。  例： `Test`

`url` 是音乐地址。  例： `http://example.org/test.mp3`

`img` 是图片地址。  例： `http://example.org/test.jpg`