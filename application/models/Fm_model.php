<?php

class Fm_model extends CI_Model {

	function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	function get($musicid)
	{
		$sql = "SELECT id, name, url, img FROM void_fm WHERE id = ?";
		$query = $this->db->query($sql, $musicid);

		if ( $query->num_rows() ) 
		{
			return $query->row_array();
		}

		return FALSE;
	}
	
	function counts()
	{
		$sql   = "SELECT COUNT(id) FROM void_fm"; 
		$query = $this->db->query($sql);

		return $query->row_array();
	}

}