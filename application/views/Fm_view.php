<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Void-FM</title>
<link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="fm.css">
</head>
<body>
<div id="player"></div>
<div id="player-wrapper">
    <div class="m-seek-bar jp-seek-bar" style="width: 100%;">
      <div class="m-play-bar jp-play-bar" style="width:0; overflow: hidden;"></div>
    </div>
    <div class="container">
    <div class="row">
      <div class="left">
        <span id="name"></span>
      </div>
      <div class="right">
        <span class="glyphicon glyphicon-random jp-repeat fm-icon" aria-hidden="true"></span>
        <span class="glyphicon glyphicon-repeat jp-repeat-off fm-icon" aria-hidden="true"></span>
        <span class="glyphicon glyphicon-play jp-play fm-icon" aria-hidden="true"></span>
        <span class="glyphicon glyphicon-pause jp-pause fm-icon" aria-hidden="true"></span>
        <span class="glyphicon glyphicon-forward fm-icon" id="next" aria-hidden="true"></span>
      </div>
    </div>
  </div>
</div>
<script src="//cdn.jsdelivr.net/jquery/2.2.4/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/jplayer/2.9.2/jquery.jplayer.min.js"></script>
<script src="fm.js"></script>
<!-- https://gitlab.com/void-hikari/void-fm -->
</body>
</html>