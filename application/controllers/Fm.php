<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fm extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('Fm_view');
	}
	
	public function get($musicid)
	{
		if(isset($musicid) && ctype_digit($musicid) && $musicid != 0)
		{
			$value = $this->fm_model->get($musicid);
			echo json_encode( array( "id" => $value['id'], "name" => $value['name'], "url" => $value['url'], "img" => $value['img'] ) );
			return;
		}

		echo json_encode( array( "id" => NULL, "name" => NULL, "url" => NULL, "img" => NULL ) );
	}
	
	public function counts()
	{
			$value = $this->fm_model->counts();
			echo json_encode( array( "counts" => $value['COUNT(id)'] ) );
	}

}